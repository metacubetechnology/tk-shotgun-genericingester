"""
Copyright (c) 2012 Shotgun Software, Inc
----------------------------------------------------

"""

import sys
import os
import platform

from tank.platform import Application
from tank import TankError

class GenericIngester(Application):
    
    def init_app(self):
        """
        Called as the application is being initialized
        """

        display_name = self.get_setting("display_name")
        deny_permissions = self.get_setting("deny_permissions")
        deny_platforms = self.get_setting("deny_platforms")
        self.folder_setting = self.get_setting("create_folder_setting")
        self.supported_extensions = self.get_setting("supported_extensions")

        properties = {
            "title": display_name,
            "deny_permissions": deny_permissions,
            "deny_platforms": deny_platforms,
            "supports_multiple_selection": True
        }

        # import using special tank import mechanism
        generic_ingester = self.import_module("generic_ingester")

        # create a callback to run when our command is launched.
        # pass the app object as a parameter.
        callback = lambda : generic_ingester.show_dialog(self, display_name)

        # add stuff to main menu
        self.engine.register_command("Generic Ingester", callback, properties)


    def copy_file(self, source_path, target_path, task):
        """
        Utility method to copy a file from source_path to
        target_path.  Uses the copy file hook specified in 
        the configuration
        """
        self.execute_hook("hook_copy_file", 
                          source_path=source_path, 
                          target_path=target_path,
                          task=task)