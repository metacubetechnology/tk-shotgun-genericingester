## tk-shotgun-genericingester

 -This tank app is intended to be a simple ingest tool to any kind of files and without many restrictions
 -The app can process more that one file, and all the logic to process each one are on hooks.

![IMAGE](raw/master/screenshot.png)
