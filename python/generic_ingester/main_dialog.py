"""
Copyright (c) 2012 Shotgun Software, Inc
----------------------------------------------------
"""
import tank
import platform
import unicodedata
import os
import sys
import pprint
from threading import Thread
import traceback
import MaxPlus
from pprint import pformat
import subprocess

from tank import TankError
from tank.platform.qt import QtCore, QtGui

from .ui.main_dialog_form import Ui_Dialog

class AppDialog(QtGui.QWidget):


    def __init__(self, app):
        QtGui.QWidget.__init__(self)

        self.setAcceptDrops(True)

        self._app = app
        self._app.log_debug('context:' + repr(self._app.context))
        self._files_to_ingest = []
        self.shotgun = self._app.tank.shotgun

        #setup from configuration
        self._main_template = self._app.get_template("main_template")
        self._app.log_debug("template: " + str(self._main_template))

        #Create Progressbar dialog
        init_progress = self.progress_bar("Please Wait", "Creating Folders...")
        init_progress.show()

        if self._app.folder_setting:
            event_loop = QtCore.QEventLoop()
            thread = FoldersThread(self._app)
            thread.finished.connect(event_loop.quit)
            thread.start()
            event_loop.exec_()

        # set up the UI
        self.ui = Ui_Dialog()

        self.ui.setupUi(self)

        # set up event listeners
        self.ui.pushButton.released.connect( self.ingest_action )

        init_progress.hide()

    def dragEnterEvent(self, event):
        """
        """
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        """
        """
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            urls = event.mimeData().urls()
            self.ui.dragArea.setCurrentIndex(1)
            self._app.log_debug(str(urls))
            for f in urls:
                #self._files_to_ingest.append(f.toLocalFile())
                file_name = os.path.basename(f.toLocalFile())

                filetype = os.path.splitext(file_name)[1]
                is_supported = False
                for ext in self._app.supported_extensions:
                    if filetype.lower() == ext:
                        is_supported = True
                        break

                if is_supported:
                    self._files_to_ingest.append(f.toLocalFile())
                    self.create_file_widget_card(file_name, f.toLocalFile())
                else:
                    #Create message here
                    msg = "The file:\n%s\nhas not a supported format of:\n%s" % (file_name, self._app.supported_extensions)
                    self.msgbox(msg)

        else:
            event.ignore()


    def progress_bar(self, title, text):

        progress = QtGui.QProgressDialog(text, 'Cancel', 0, 0, self)
        progress.setCancelButton(None)
        progress.setWindowModality(QtCore.Qt.WindowModal)
        progress.setRange(0,0)
        progress.resize(300,100)
        progress.setWindowTitle(title)

        return progress


    def create_file_widget_card(self, file_name, file_path):

        fWidget = QtGui.QFrame(self.ui.files_frame)
        fWidget.setObjectName("fWidget_" + file_name)
        fWidget.setStyleSheet("background-color: rgb(100, 100, 100);")
        fWidget_Layout = QtGui.QVBoxLayout(fWidget)
        fWidget_Layout.setObjectName("fWidget_Layout_" + file_name)
        fMainLayout = QtGui.QHBoxLayout()
        fMainLayout.setObjectName("fMainLayout_" + file_name)
        fIcon = QtGui.QLabel(fWidget)
        fIcon.setText("")
        fIcon.setMaximumSize(QtCore.QSize(50, 80))
        fIcon.setPixmap(QtGui.QPixmap(":/res/work_file.png"))
        fIcon.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        fIcon.setObjectName("fIcon_" + file_name)
        fMainLayout.addWidget(fIcon)
        fInfoLayout = QtGui.QVBoxLayout()
        fInfoLayout.setObjectName("fInfoLayout_" + file_name)
        fText = QtGui.QLabel(fWidget)
        fText.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        fText.setObjectName("fText_" + file_name)
        fText.setText("file name:")
        fInfoLayout.addWidget(fText)
        fText2 = QtGui.QLabel(fWidget)
        fText2.setObjectName("fText2_" + file_name)
        fText2.setText(file_name)
        fInfoLayout.addWidget(fText2)
        fMainLayout.addLayout(fInfoLayout)
        fDestroy = QtGui.QCommandLinkButton(fWidget)
        fDestroy.setMaximumSize(QtCore.QSize(50, 50))
        fDestroy.setText("")
        fDestroyIcon = QtGui.QIcon()
        fDestroyIcon.addPixmap(QtGui.QPixmap(":/res/delete.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        fDestroy.setIcon(fDestroyIcon)
        fDestroy.setIconSize(QtCore.QSize(35, 35))
        fDestroy.setObjectName("fDestroy_" + file_name)
        fDestroy.released.connect(lambda : self.destroy_fwidget(fWidget, file_path))
        fMainLayout.addWidget(fDestroy)
        fWidget_Layout.addLayout(fMainLayout)
        self.ui.files_scroll_contents_layout.addWidget(fWidget)


    def destroy_fwidget(self, widget, file_path):
        self._files_to_ingest.remove(file_path)
        widget.deleteLater()


    def hook_finished(self, data):

        self._hook_output = data['output']
        self._hook_errors = data['errors']

    def ingest_action(self):

        try:

            #Create Progressbar dialog
            hook_progress = self.progress_bar("Please Wait", "Procesing your files...")
            hook_progress.show()

            '''
            event_loop = QtCore.QEventLoop()
            thread = HookThread(self._app, self._app.context, self._main_template, self._files_to_ingest)

            thread._finish.signal.connect(self.hook_finished)

            thread.finished.connect(event_loop.quit)
            thread.start()
            event_loop.exec_()


            '''

            ingest_data = self.process_ingest(self._app, self._app.context, self._main_template, self._files_to_ingest)
            self._hook_output = ingest_data['output']
            self._hook_errors = ingest_data['errors']

            hook_progress.hide()

            self.ui.pushButton.released.disconnect( self.ingest_action )

            self.ui.pushButton.released.connect( self.close )
            self.ui.pushButton.setText('Close')


            if len(self._hook_errors) != 0:

                self.ui.dragArea.setCurrentIndex(3)
                self.ui.code_traceback.setText(self._hook_errors[0])

            else:

                self.ui.dragArea.setCurrentIndex(2)

                '''
                QtGui.QMessageBox.information(self,
                                              'Info...',
                                              str(self._hook_output),
                                              QtGui.QMessageBox.Ok)
                '''
        except TankError, e:
            Error = traceback.format_exc()
            self.ui.dragArea.setCurrentIndex(3)
            self.ui.code_traceback.setText(Error)

    def process_ingest(self, app, context, main_template, files_list):
        output = []
        errors = []

        try:
            print 'Starting ingest...'

            app.log_debug('Starting Hook')
            seq = 1
            files_list.sort()
            for item in files_list:
                print 'x--> ', item
                source_path = str(item)

                #Obtain name and ext from file
                file_name = os.path.basename(source_path)
                name, ext = file_name.split('.')

                #get scene context for complete preload template
                scene_path = MaxPlus.Core.EvalMAXScript('maxfilepath').Get()

                if not scene_path:
                    raise TankError("Please Save your file before Preloading")
                name = MaxPlus.Core.EvalMAXScript ('maxfilename').Get()
                scene_template = context.sgtk.template_from_path(scene_path + os.path.sep + name)
                scene_fields = scene_template.get_fields(scene_path + os.path.sep + name)

                # get the current context, rendered for this template
                fields = context.as_template_fields(main_template)
                fields["name"] = name.lower().replace(' ', '-')
                fields["ext"] = 'png'
                fields["SEQ"] = seq
                fields["version"] = scene_fields["version"]
                fields_str = pformat(fields)
                seq += 1

                # turn the fields into a path
                target_path = main_template.apply_fields(fields)

                dirname = os.path.dirname(target_path)
                if not os.path.isdir(dirname):
                    old_umask = os.umask(0)
                    os.makedirs(dirname, 0777)
                    os.umask(old_umask)

                output.append(target_path)

                #app.copy_file(source_path, target_path, context.task)

                #rescale images with letterboxing
                scale_cmd = "scale=iw*min(%(width)s/iw\,%(height)s/ih):ih*min(%(width)s/iw\,%(height)s/ih), "
                scale_cmd += "pad=%(width)s:%(height)s:(%(width)s-iw*min(%(width)s/iw\,%(height)s/ih))/2:(%(height)s-ih*min(%(width)s/iw\,%(height)s/ih))/2"
                scale_cmd = scale_cmd % {'width': str(1920), 'height': str(1080)}

                convert_cmd = ['ffmpeg', '-i', source_path, '-y', '-filter:v', scale_cmd, target_path ]

                print 'convert_cmd', convert_cmd
                transcode_images = subprocess.Popen(convert_cmd, startupinfo = subprocess.STARTUPINFO(),
                                                   stdout=subprocess.PIPE,
                                                   stderr=subprocess.PIPE,
                                                   stdin=subprocess.PIPE)

                stdout, stderr = transcode_images.communicate()

                if transcode_images.returncode != 0:
                    raise Exception("Failed to transcode images: %s" % (str(stderr) + '\n' + str(stdout) + '\n' + str(convert_cmd)))

        except Exception, e:
            print 'error: ', e

            Error = traceback.format_exc()
            errors.append(str(e) + '\n' + str(Error))


        return {'output': output, 'errors': errors}

    def msgbox(msg):
        if sys.platform == "win32":
            import ctypes
            MessageBox = ctypes.windll.user32.MessageBoxA
            MessageBox(None, msg, "Warning", 0)





class FoldersThread(QtCore.QThread):
    """
    Simple worker thread that encapsulates folders creation.
    Broken out of the main loop so that the UI can remain responsive
    even though an process is happening
    """
    def __init__(self, app):
        QtCore.QThread.__init__(self)

        self._entityType = app.context.entity['type']
        self._entityID = app.context.entity['id']

    def run(self):
        """
        Thread loop
        """
        tk = tank.tank_from_entity(self._entityType, self._entityID)
        tk.create_filesystem_structure(self._entityType, self._entityID, None)


class HookThread(QtCore.QThread):
    """
    Simple worker thread that encapsulates hook execution.
    Broken out of the main loop so that the UI can remain responsive
    even though an process is happening
    """
    def __init__(self, app, context, main_template, files_list):
        QtCore.QThread.__init__(self)

        self._finish = HookSignal()

        self._context = context
        self._main_template = main_template
        self._files_list = files_list
        self._app = app

    def run(self):
        """
        Thread loop
        """
        try:

            hook = self._app.execute_hook("main_hook",
                                           app = self._app,
                                           context = self._context,
                                           main_template = self._main_template,
                                           files_list = self._files_list)
        except Exception, e:
            Error = traceback.format_exc()
            hook = {'output': [], 'errors': [str(e) + '\n' + str(Error)]}

        self._app.log_debug('HookData: ' + str(hook))

        self._finish.signal.emit(hook)


class HookSignal(QtCore.QObject):
    signal = QtCore.Signal(dict)


