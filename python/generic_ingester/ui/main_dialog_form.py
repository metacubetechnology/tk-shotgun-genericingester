# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_dialog_form.ui'
#
# Created: Fri May 15 09:59:34 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(575, 391)
        Dialog.setStyleSheet("")
        self.verticalLayout = QtGui.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.dragArea = QtGui.QStackedWidget(Dialog)
        self.dragArea.setLineWidth(5)
        self.dragArea.setObjectName("dragArea")
        self.drag_page = QtGui.QWidget()
        self.drag_page.setObjectName("drag_page")
        self.drag_page_layout = QtGui.QHBoxLayout(self.drag_page)
        self.drag_page_layout.setObjectName("drag_page_layout")
        self.drag_frame = QtGui.QFrame(self.drag_page)
        self.drag_frame.setStyleSheet("#drag_frame {\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-radius: 2px;\n"
"border-color: rgb(32,32,32);\n"
"}")
        self.drag_frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.drag_frame.setFrameShadow(QtGui.QFrame.Raised)
        self.drag_frame.setObjectName("drag_frame")
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.drag_frame)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label = QtGui.QLabel(self.drag_frame)
        self.label.setStyleSheet("font: 22pt \"Helvetica\";")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout_3.addWidget(self.label)
        self.drag_page_layout.addWidget(self.drag_frame)
        self.dragArea.addWidget(self.drag_page)
        self.files_page = QtGui.QWidget()
        self.files_page.setObjectName("files_page")
        self.files_page_layout = QtGui.QHBoxLayout(self.files_page)
        self.files_page_layout.setObjectName("files_page_layout")
        self.files_frame = QtGui.QFrame(self.files_page)
        self.files_frame.setStyleSheet("#files_frame {\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-radius: 2px;\n"
"border-color: rgb(32,32,32);\n"
"}")
        self.files_frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.files_frame.setFrameShadow(QtGui.QFrame.Raised)
        self.files_frame.setObjectName("files_frame")
        self.files_frame_layout = QtGui.QVBoxLayout(self.files_frame)
        self.files_frame_layout.setObjectName("files_frame_layout")
        self.files_frame_label_layout = QtGui.QHBoxLayout()
        self.files_frame_label_layout.setObjectName("files_frame_label_layout")
        self.files_frame_label_text = QtGui.QLabel(self.files_frame)
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        font.setPointSize(26)
        font.setWeight(50)
        font.setItalic(False)
        font.setBold(False)
        self.files_frame_label_text.setFont(font)
        self.files_frame_label_text.setStyleSheet("font: 26pt \"Helvetica\";")
        self.files_frame_label_text.setAlignment(QtCore.Qt.AlignCenter)
        self.files_frame_label_text.setObjectName("files_frame_label_text")
        self.files_frame_label_layout.addWidget(self.files_frame_label_text)
        self.files_frame_layout.addLayout(self.files_frame_label_layout)
        self.files_scroll = QtGui.QScrollArea(self.files_frame)
        self.files_scroll.setWidgetResizable(True)
        self.files_scroll.setObjectName("files_scroll")
        self.files_scroll_contents = QtGui.QWidget()
        self.files_scroll_contents.setGeometry(QtCore.QRect(0, 0, 517, 247))
        self.files_scroll_contents.setObjectName("files_scroll_contents")
        self.files_scroll_contents_layout = QtGui.QVBoxLayout(self.files_scroll_contents)
        self.files_scroll_contents_layout.setObjectName("files_scroll_contents_layout")
        self.files_scroll.setWidget(self.files_scroll_contents)
        self.files_frame_layout.addWidget(self.files_scroll)
        self.files_page_layout.addWidget(self.files_frame)
        self.dragArea.addWidget(self.files_page)
        self.succes_page = QtGui.QWidget()
        self.succes_page.setObjectName("succes_page")
        self.succes_page_layout = QtGui.QHBoxLayout(self.succes_page)
        self.succes_page_layout.setObjectName("succes_page_layout")
        self.succes_frame = QtGui.QFrame(self.succes_page)
        self.succes_frame.setStyleSheet("#drag_frame {\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-radius: 2px;\n"
"border-color: rgb(32,32,32);\n"
"}")
        self.succes_frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.succes_frame.setFrameShadow(QtGui.QFrame.Raised)
        self.succes_frame.setObjectName("succes_frame")
        self.succes_layout = QtGui.QVBoxLayout(self.succes_frame)
        self.succes_layout.setSpacing(0)
        self.succes_layout.setContentsMargins(0, 0, 0, 0)
        self.succes_layout.setObjectName("succes_layout")
        self.succes_icon = QtGui.QLabel(self.succes_frame)
        self.succes_icon.setText("")
        self.succes_icon.setPixmap(QtGui.QPixmap(":/res/success.png"))
        self.succes_icon.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignHCenter)
        self.succes_icon.setObjectName("succes_icon")
        self.succes_layout.addWidget(self.succes_icon)
        self.succes_text = QtGui.QLabel(self.succes_frame)
        self.succes_text.setMinimumSize(QtCore.QSize(0, 0))
        self.succes_text.setMaximumSize(QtCore.QSize(16777215, 130))
        self.succes_text.setStyleSheet("font: 22pt \"Helvetica\";")
        self.succes_text.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)
        self.succes_text.setObjectName("succes_text")
        self.succes_layout.addWidget(self.succes_text)
        self.succes_page_layout.addWidget(self.succes_frame)
        self.dragArea.addWidget(self.succes_page)
        self.failed_page = QtGui.QWidget()
        self.failed_page.setObjectName("failed_page")
        self.failed_page_layout = QtGui.QHBoxLayout(self.failed_page)
        self.failed_page_layout.setObjectName("failed_page_layout")
        self.failed_frame = QtGui.QFrame(self.failed_page)
        self.failed_frame.setStyleSheet("#drag_frame {\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-radius: 2px;\n"
"border-color: rgb(32,32,32);\n"
"}")
        self.failed_frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.failed_frame.setFrameShadow(QtGui.QFrame.Raised)
        self.failed_frame.setObjectName("failed_frame")
        self.failed_layout = QtGui.QVBoxLayout(self.failed_frame)
        self.failed_layout.setSpacing(0)
        self.failed_layout.setContentsMargins(0, 0, 0, 0)
        self.failed_layout.setObjectName("failed_layout")
        self.failed_icon = QtGui.QLabel(self.failed_frame)
        self.failed_icon.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.failed_icon.setText("")
        self.failed_icon.setPixmap(QtGui.QPixmap(":/res/failure.png"))
        self.failed_icon.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignHCenter)
        self.failed_icon.setObjectName("failed_icon")
        self.failed_layout.addWidget(self.failed_icon)
        self.failed_text = QtGui.QLabel(self.failed_frame)
        self.failed_text.setMinimumSize(QtCore.QSize(0, 0))
        self.failed_text.setMaximumSize(QtCore.QSize(16777215, 80))
        self.failed_text.setStyleSheet("font: 22pt \"Helvetica\";")
        self.failed_text.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)
        self.failed_text.setObjectName("failed_text")
        self.failed_layout.addWidget(self.failed_text)
        self.code_traceback = QtGui.QLabel(self.failed_frame)
        self.code_traceback.setScaledContents(False)
        self.code_traceback.setWordWrap(False)
        self.code_traceback.setMargin(30)
        self.code_traceback.setIndent(-1)
        self.code_traceback.setObjectName("code_traceback")
        self.failed_layout.addWidget(self.code_traceback)
        self.failed_page_layout.addWidget(self.failed_frame)
        self.dragArea.addWidget(self.failed_page)
        self.verticalLayout.addWidget(self.dragArea)
        self.main_layout = QtGui.QHBoxLayout()
        self.main_layout.setSpacing(6)
        self.main_layout.setObjectName("main_layout")
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.main_layout.addItem(spacerItem)
        self.pushButton = QtGui.QPushButton(Dialog)
        self.pushButton.setObjectName("pushButton")
        self.main_layout.addWidget(self.pushButton)
        self.verticalLayout.addLayout(self.main_layout)

        self.retranslateUi(Dialog)
        self.dragArea.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Generic Ingester", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Dialog", "Drag files here", None, QtGui.QApplication.UnicodeUTF8))
        self.files_frame_label_text.setText(QtGui.QApplication.translate("Dialog", "Files to Ingest:", None, QtGui.QApplication.UnicodeUTF8))
        self.succes_text.setText(QtGui.QApplication.translate("Dialog", "All files were ingested successfully", None, QtGui.QApplication.UnicodeUTF8))
        self.failed_text.setText(QtGui.QApplication.translate("Dialog", "Ingestion Failure!!!", None, QtGui.QApplication.UnicodeUTF8))
        self.code_traceback.setText(QtGui.QApplication.translate("Dialog", "Code Traceback", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton.setText(QtGui.QApplication.translate("Dialog", "Ingest", None, QtGui.QApplication.UnicodeUTF8))

from . import resources_rc
from . import resources_rc
