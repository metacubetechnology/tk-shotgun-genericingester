"""
Copyright (c) 2012 Shotgun Software, Inc
----------------------------------------------------

"""
def show_dialog(app, display_name):
    # defer imports so that the app works gracefully in batch modes
    from .main_dialog import AppDialog
    # show the dialog window using the engine's show_dialog method
    app.engine.show_dialog(display_name, app, AppDialog, app)
