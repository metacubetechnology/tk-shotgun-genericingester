# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import os
import traceback

import tank
from tank import Hook
from tank import TankError

from pprint import pformat

class MainHook(Hook):
    """
    Hook called when a file needs to be copied
    """
    
    def execute(self, app, context, main_template, files_list):
        """
        Main hook entry point
                        
        :returns:       errors
        """
        output = []
        errors = []

        try:
            app.log_debug('Starting Hook')

            for item in files_list:

                source_path = str(item)

                #Obtain name and ext from file
                file_name = os.path.basename(source_path)
                name, ext = file_name.split('.')

                # get the current context, rendered for this template
                fields = context.as_template_fields(main_template)
                fields["name"] = name.lower().replace(' ', '-')
                fields["ext"] = ext.lower()
                fields_str = pformat(fields)

                # turn the fields into a path
                target_path = main_template.apply_fields(fields)
                output.append(target_path)

                app.copy_file(source_path, target_path, context.task)


        except Exception, e:
            Error = traceback.format_exc()
            errors.append(str(e) + '\n' + str(Error))

        return {'output': output, 'errors': errors}
