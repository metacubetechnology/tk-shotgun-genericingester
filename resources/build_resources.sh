#!/usr/bin/env bash
# 
# Copyright (c) 2008 Shotgun Software, Inc
# ----------------------------------------------------

echo "building user interfaces..."
pyside-uic --from-imports main_dialog_form.ui > ../python/generic_ingester/ui/main_dialog_form.py

echo "building resources..."
pyside-rcc resources.qrc > ../python/generic_ingester/ui/resources_rc.py
